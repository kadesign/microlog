export default class MicroLog {
  /**
   * @param {string|object} module - Module or module name
   */
  constructor(module = 'main') {
    if (module instanceof Object) {
      this._moduleName = module.constructor.name;
    } else if (typeof module === 'string') {
      this._moduleName = module;
    } else {
      throw new TypeError('Either class instance or string can be passed as argument');
    }
    this._outputDateInUTC = false;
  }

  /**
   * Sets specific module name to logger
   *
   * @param {string} moduleName - Module name
   * @returns {MicroLog} - This instance of MicroLog
   */
  setModuleName(moduleName) {
    this._moduleName = moduleName;
    return this;
  }

  /**
   * Sets logger to output the date in UTC time zone
   *
   * @returns {MicroLog} - This instance of MicroLog
   */
  setOutputDateInUTC() {
    this._outputDateInUTC = true;
    return this;
  }

  /**
   * Log message with generic log level ('log')
   *
   * @param {...any} msg - Message parts
   */
  log(...msg) {
    this._logWithLevel('log', msg);
  };

  /**
   * Log message with log level 'trace'
   *
   * @param {...any} msg - Message parts
   */
  trace(...msg) {
    this._logWithLevel('trace', msg);
  };

  /**
   * Log message with log level 'debug'
   *
   * @param {...any} msg - Message parts
   */
  debug(...msg) {
    this._logWithLevel('debug', msg);
  };

  /**
   * Log message with log level 'info'
   *
   * @param {...any} msg - Message parts
   */
  info(...msg) {
    this._logWithLevel('info', msg);
  };

  /**
   * Log message with log level 'warn'
   *
   * @param {...any} msg - Message parts
   */
  warn(...msg) {
    this._logWithLevel('warn', msg);
  };

  /**
   * Log error or any message with log level 'error'
   *
   * @param {...any|Error} err - Message parts
   */
  error(...err) {
    if (err.length === 0) return;

    if (err[0] instanceof Error) {
      console.log(`${this._getPrefix('error')} ${err[0].message}`);
      console.log(err[0].stack);
      if (err[0].cause) {
        console.log(`Caused by: ${err[0].cause.stack}`);
      }
    } else {
      this._logWithLevel('error', err);
    }
  };

  _logWithLevel(logLevel, msg) {
    if (msg.length === 0) return;

    console.log(`${this._getPrefix(logLevel)} ${this._prepareMessage(msg)}`);
  }

  _getPrefix(logLevel) {
    return `${this._getCurrentTimestamp()} [ ${this._padLogLevel(logLevel)} ] <${this._moduleName}>`;
  }

  _padLogLevel(logLevel) {
    const len = 5;
    const pad = (len - logLevel.length) / 2;
    return new Array(Math.floor(pad)).fill(' ').join('')
      + logLevel
      + new Array(Math.ceil(pad)).fill(' ').join('');
  }

  _prepareMessage(msg) {
    return msg.map(part => part instanceof Object ? JSON.stringify(part) : part).join(' ');
  }

  _getCurrentTimestamp() {
    const date = new Date();
    if (this._outputDateInUTC) {
      return date.getUTCFullYear() + '-' + (('0' + (date.getUTCMonth() + 1)).slice(-2)) + '-' +
        (('0' + date.getUTCDate()).slice(-2)) + ' ' + (('0' + date.getUTCHours()).slice(-2)) + ':' +
        (('0' + date.getUTCMinutes()).slice(-2)) + ':' + (('0' + date.getUTCSeconds()).slice(-2));
    } else {
      return date.getFullYear() + '-' + (('0' + (date.getMonth() + 1)).slice(-2)) + '-' +
        (('0' + date.getDate()).slice(-2)) + ' ' + (('0' + date.getHours()).slice(-2)) + ':' +
        (('0' + date.getMinutes()).slice(-2)) + ':' + (('0' + date.getSeconds()).slice(-2));
    }
  };
}
