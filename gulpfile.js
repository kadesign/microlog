import gulp from 'gulp';
import babel from 'gulp-babel';
import del from 'del';

const ESM_DIR = 'esm';
const CJS_DIR = 'cjs';

gulp.task('convert-to-cjs', () => {
  const src = `${ESM_DIR}/*.js`;
  const dest = `${CJS_DIR}`;
  return gulp
    .src(src)
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(gulp.dest(dest));
});

gulp.task('clean', () => {
  return del([ `${CJS_DIR}/*.js`, `!${CJS_DIR}/package.json` ]);
});

gulp.task('default', gulp.series('clean', 'convert-to-cjs'));