# Changelog

## 0.4.1
* Added logging error with cause

## 0.4.0
* Added JSDoc comments
* Added more log levels (log, trace, info)
* Added objects logging
* Added logging message with multiple parts

## 0.3.0
* Change build process - CJS version is generated from ESM. CJS version should be imported as `require('@kadesign/microlog').default`

## 0.2.0
* CJS version implemented - use `require('@kadesign/microlog/cjs')` for importing the logger into a project
* Package is now published under [MIT license](LICENSE)

## 0.1.2
* Module object/name can be passed to constructor to set module name in output in addition to method `setModuleName()`

## 0.1.1
* Minimal supported node version upgraded to 13.2.0

## 0.1.0
* Initial release
