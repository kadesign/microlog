import MicroLog from '../esm/index.js';

function logMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.log(msg);
}

function logMessagePartsWithMainModuleName(msgPart1, msgPart2) {
  const logger = new MicroLog();
  logger.log(msgPart1, msgPart2);
}

function logObjectWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.log({msg});
}

function logTextAndObjectWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.log(msg, {msg});
}

function logTraceMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.trace(msg);
}

function logDebugMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.debug(msg);
}

function logInfoMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.info(msg);
}

function logWarningMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.warn(msg);
}

function logErrorWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.error(new Error(msg));
}

function logErrorWithMainModuleNameAndCause(msg, cause) {
  const logger = new MicroLog();
  logger.error(new Error(msg, { cause: new Error(cause) }));
}

function logErrorMessageWithMainModuleName(msg) {
  const logger = new MicroLog();
  logger.error(msg);
}

function logMessageWithModuleNameAsStringFromMethod(module, msg) {
  const logger = new MicroLog().setModuleName(module);
  logger.log(msg);
}

function logMessageWithModuleNameAsStringFromConstructor(module, msg) {
  const logger = new MicroLog(module);
  logger.log(msg);
}

function logMessageWithModuleNameFromObject(msg) {
  const SomeObject = class SomeObject {
    constructor() {
      this.microlog = new MicroLog(this);
    }

    logMessage(str) {
      this.microlog.log(str);
    }
  }

  new SomeObject().logMessage(msg);
}

function tryLogEmptyMessage() {
  const logger = new MicroLog();
  logger.log();
}

function tryLogEmptyError() {
  const logger = new MicroLog();
  logger.error();
}

function logMessageWithUTCDateTime() {
  const logger = new MicroLog().setOutputDateInUTC();
  logger.log('message');
}

function throwError() {
  try {
    new MicroLog(1);
  } catch (e) {
    console.error(e);
  }
}

switch (process.argv[2]) {
  case 'log':
    logMessageWithMainModuleName(process.argv[3]);
    break;
  case 'logParts':
    logMessagePartsWithMainModuleName(process.argv[3], process.argv[4]);
    break;
  case 'logObject':
    logObjectWithMainModuleName(process.argv[3]);
    break;
  case 'logTxtObject':
    logTextAndObjectWithMainModuleName(process.argv[3]);
    break;
  case 'trace':
    logTraceMessageWithMainModuleName(process.argv[3]);
    break;
  case 'debug':
    logDebugMessageWithMainModuleName(process.argv[3]);
    break;
  case 'info':
    logInfoMessageWithMainModuleName(process.argv[3]);
    break;
  case 'warning':
    logWarningMessageWithMainModuleName(process.argv[3]);
    break;
  case 'errorErr':
    logErrorWithMainModuleName(process.argv[3]);
    break;
  case 'errorErrWithCause':
    logErrorWithMainModuleNameAndCause(process.argv[3], process.argv[4]);
    break;
  case 'errorMsg':
    logErrorMessageWithMainModuleName(process.argv[3]);
    break;
  case 'moduleStrMethod':
    logMessageWithModuleNameAsStringFromMethod(process.argv[3], process.argv[4]);
    break;
  case 'moduleStrConstructor':
    logMessageWithModuleNameAsStringFromConstructor(process.argv[3], process.argv[4]);
    break;
  case 'moduleObj':
    logMessageWithModuleNameFromObject(process.argv[3]);
    break;
  case 'emptyMsg':
    tryLogEmptyMessage();
    break;
  case 'emptyErr':
    tryLogEmptyError();
    break;
  case 'utc':
    logMessageWithUTCDateTime();
    break;
  case 'error':
    throwError();
    break;
}
