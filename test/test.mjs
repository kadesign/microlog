import util from 'util';
import path from 'path';
import chai from 'chai';
import { exec as originalExec } from 'child_process';

const expect = chai.expect;
const exec = util.promisify(originalExec);
const testPath = path.resolve(process.cwd(), 'test', 'script.mjs');
const cmd = (...args) => args.join(' ');

describe('Console output', () => {
  let loggedStr;
  let moduleName;

  beforeEach(() => {
    loggedStr = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    moduleName = Math.random().toString(36).substring(2, 15);
  });

  it('Log message', async () => {
    const expectedString = `[  log  ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'log', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Log message parts', async () => {
    const expectedString = `[  log  ] <main> ${loggedStr} 1`;
    const { stdout } = await exec(cmd('node', testPath, 'logParts', loggedStr, 1));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Log object', async () => {
    const expectedString = `[  log  ] <main> {"msg":"${loggedStr}"}`;
    const { stdout } = await exec(cmd('node', testPath, 'logObject', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Log text and object', async () => {
    const expectedString = `[  log  ] <main> ${loggedStr} {"msg":"${loggedStr}"}`;
    const { stdout } = await exec(cmd('node', testPath, 'logTxtObject', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Trace message', async () => {
    const expectedString = `[ trace ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'trace', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Debug message', async () => {
    const expectedString = `[ debug ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'debug', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Info message', async () => {
    const expectedString = `[ info  ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'info', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Warning message', async () => {
    const expectedString = `[ warn  ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'warning', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Error message from text', async () => {
    const expectedString = `[ error ] <main> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'errorMsg', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Error message from error', async () => {
    const expectedString = `[ error ] <main> ${loggedStr}\nError: ${loggedStr}\n    at `;
    const { stdout } = await exec(cmd('node', testPath, 'errorErr', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Error message from error with cause', async () => {
    const expectedString = `[ error ] <main> ${loggedStr}\nError: ${loggedStr}\n    at `;
    const loggedCause = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    const expectedCause = `Caused by: Error: ${loggedCause}\n    at `;
    const { stdout } = await exec(cmd('node', testPath, 'errorErrWithCause', loggedStr, loggedCause));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString)
      .and.to.contain(expectedCause);
  });
  it('Message with custom module name set with .setModuleName(string)', async () => {
    const expectedString = `[  log  ] <${moduleName}> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'moduleStrMethod', moduleName, loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Message with custom module name set with constructor(string)', async () => {
    const expectedString = `[  log  ] <${moduleName}> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'moduleStrConstructor', moduleName, loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Message with custom module name set with constructor(object)', async () => {
    const expectedString = `[  log  ] <SomeObject> ${loggedStr}`;
    const { stdout } = await exec(cmd('node', testPath, 'moduleObj', loggedStr));

    console.log('Stdout: ', stdout);
    expect(stdout).to.contain(expectedString);
  });
  it('Empty message', async () => {
    const { stdout } = await exec(cmd('node', testPath, 'emptyMsg'));

    console.log('Stdout: ', stdout);
    expect(stdout).to.be.empty;
  });
  it('Empty error', async () => {
    const { stdout } = await exec(cmd('node', testPath, 'emptyErr'));

    console.log('Stdout: ', stdout);
    expect(stdout).to.be.empty;
  });
  it('Message with UTC date and time', async () => {
    const now = new Date();
    const dateTimeInUTC = now.getUTCFullYear() + '-' + (('0' + (now.getUTCMonth() + 1)).slice(-2)) + '-' +
      (('0' + now.getUTCDate()).slice(-2)) + ' ' + (('0' + now.getUTCHours()).slice(-2)) + ':' +
      (('0' + now.getUTCMinutes()).slice(-2)) + ':' + (('0' + now.getUTCSeconds()).slice(-2));

    const expectedString = `${dateTimeInUTC} [  log  ] <main> message`;
    const { stdout } = await exec(cmd('node', testPath, 'utc'));

    console.log('Stdout: ', stdout);
    expect(stdout.startsWith(expectedString)).to.be.true;
  });
  it('Erroneous logger instantiation', async () => {
    const expectedString = 'TypeError: Either class instance or string can be passed as argument';
    const { stderr } = await exec(cmd('node', testPath, 'error'));

    console.log('Stderr: ', stderr);
    expect(stderr.startsWith(expectedString)).to.be.true;
  });
});
