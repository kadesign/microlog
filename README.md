# MicroLog
[![Go to NPM package](https://img.shields.io/npm/v/@kadesign/microlog?label=version)](https://www.npmjs.com/package/@kadesign/microlog)
[![Pipeline status](https://gitlab.com/kadesign/microlog/badges/master/pipeline.svg)](https://gitlab.com/kadesign/microlog/-/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/kadesign/microlog/badge.svg?branch=master)](https://coveralls.io/gitlab/kadesign/toolbox-js?branch=master)
![Maintenance status](https://img.shields.io/maintenance/yes/2023)

Tiny logger for Node applications

## Usage

```
import MicroLog from '@kadesign/microlog';              // ESM
const MicroLog = require('@kadesign/microlog').default; // CommonJS

const SomeObject = class SomeObject {
  constructor() {
    this.microlog = new MicroLog(this);
  }

  logMessage(str) {
    this.microlog.log(str);
  }
}

const MainLogger = new MicroLog().setOutputDateInUTC();
const ModuleLogger = new MicroLog().setModuleName('some-module');
const ModuleLoggerStr = new MicroLog('anotherModule');

MainLogger.log('Some message');
ModuleLogger.warn('Warning message');
MainLogger.error(new Error('Sample error'));

ModuleLoggerStr.log('This logger is instantiated with string as argument');
new SomeObject().logMessage('This logger is instantiated with object as argument');
```

**Output**

```
2020-01-01 12:00:00 [  log  ] <main> Some message
2020-01-01 14:00:01 [ warn  ] <some-module> Warning message
2020-01-01 12:00:01 [ error ] <main> Sample error
Error: Sample error
    at file:///path/to/some/file.js:8:18
    at ModuleJob.run (internal/modules/esm/module_job.js:110:37)
    at async Loader.import (internal/modules/esm/loader.js:164:24)
2020-01-01 12:00:01 [  log  ] <anotherModule> This logger is instantiated with string as argument
2020-01-01 12:00:01 [  log  ] <SomeObject> This logger is instantiated with object as argument
```
